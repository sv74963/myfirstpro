package com.sandeep.moviecatalogservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sandeep.moviecatalogservice.service.MovieCatalogService;

@RestController
@RequestMapping(value="/catalog")
public class MovieCatalogController {
	@Autowired
	@Qualifier("movieCatalogServiceImplTwo")  // to Identetify which child bean would be going to use.
	MovieCatalogService movieCatalogServiceImplTwo;
	
	@Autowired
	MovieCatalogService movieCatalogServiceImpl; // MovieCatalogServiceImpl is primary bean so no need to use @Qualifier.
	
	@RequestMapping(value="/getMovieCatalog",method=RequestMethod.GET)
	public String getMovieCatalog(){
		return movieCatalogServiceImpl.getMovieCatalog();
		
	}

	@RequestMapping(value="/getMovieCatalogTwo",method=RequestMethod.GET)
	public String getMovieCatalogTwo(){
		return movieCatalogServiceImplTwo.getMovieCatalog();
		
	}
}
