package com.sandeep.moviecatalogservice.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service("movieCatalogServiceImpl")
@Primary // it will be default bean  when its parent will be autowired.
public class MovieCatalogServiceImpl implements MovieCatalogService {

	@Override
	public String getMovieCatalog() {
		return "MovieCatalogServiceImpl";
	}

}
