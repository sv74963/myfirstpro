package com.sandeep.moviecatalogservice.service;

import org.springframework.stereotype.Service;

@Service("movieCatalogServiceImplTwo")
public class MovieCatalogServiceImplTwo implements MovieCatalogService{

	@Override
	public String getMovieCatalog() {
		return "MovieCatalogServiceImplTwo";
	}

}
